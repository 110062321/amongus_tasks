const {ccclass, property} = cc._decorator;
@ccclass
export default class mario extends cc.Component {
    @property(cc.Node)
    flag: cc.Node = null;
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;
    @property(cc.AudioClip)
    moveSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    arriveSE: cc.AudioClip = null;

    move:number;
    count:number;

    start(){
        this.move=0;
        this.count=0;
        this.playBGM();
    }
    onLoad(){
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }
    update(dt){
        if(this.count==33){
            cc.audioEngine.stopMusic();
            cc.audioEngine.playEffect(this.arriveSE, false);
            cc.director.loadScene("default_end");
        }
    }
    onKeyDown(e){
        if(e.keyCode==cc.macro.KEY.space&&!this.move){
            this.node.x+=25;
            this.move=1;
            this.count++;
            cc.audioEngine.playEffect(this.moveSE, false);
        }
    }
    onKeyUp(e){
        if(e.keyCode==cc.macro.KEY.space){
            this.move=0;
        }
    }
    playBGM(){
        cc.audioEngine.playMusic(this.bgm,true);
    }
}
