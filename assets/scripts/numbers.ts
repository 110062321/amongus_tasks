const {ccclass, property} = cc._decorator;
@ccclass
export default class numbers extends cc.Component {
    @property(cc.Button)
    n1:cc.Button=null;
    @property(cc.Button)
    n2:cc.Button=null;
    @property(cc.Button)
    n3:cc.Button=null;
    @property(cc.Button)
    n4:cc.Button=null;
    @property(cc.Button)
    n5:cc.Button=null;
    @property(cc.Button)
    n6:cc.Button=null;
    @property(cc.Button)
    n7:cc.Button=null;
    @property(cc.Button)
    n8:cc.Button=null;
    @property(cc.Button)
    n9:cc.Button=null;
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;
    @property(cc.AudioClip)
    rightSE: cc.AudioClip = null;
    @property(cc.AudioClip)
    wrongSE: cc.AudioClip = null;

    pressed:number;

    start(){
        this.pressed=0;
        this.playBGM();
    }
    onLoad(){
        this.n1.node.on(cc.Node.EventType.TOUCH_END, this.n1Click, this);
        this.n2.node.on(cc.Node.EventType.TOUCH_END, this.n2Click, this);
        this.n3.node.on(cc.Node.EventType.TOUCH_END, this.n3Click, this);
        this.n4.node.on(cc.Node.EventType.TOUCH_END, this.n4Click, this);
        this.n5.node.on(cc.Node.EventType.TOUCH_END, this.n5Click, this);
        this.n6.node.on(cc.Node.EventType.TOUCH_END, this.n6Click, this);
        this.n7.node.on(cc.Node.EventType.TOUCH_END, this.n7Click, this);
        this.n8.node.on(cc.Node.EventType.TOUCH_END, this.n8Click, this);
        this.n9.node.on(cc.Node.EventType.TOUCH_END, this.n9Click, this);
    }
    update(dt){
        if(this.pressed==9){
            cc.audioEngine.stopMusic();
            cc.director.loadScene("default_end");
        }
    }
    n1Click(){
        if(!this.pressed){
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else{
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed=0;
        }
    }
    n2Click(){
        if(this.pressed==1){
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else{
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed=0;
        }
    }
    n3Click(){
        if(this.pressed==2){
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else{
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed=0;
        }
    }
    n4Click(){
        if(this.pressed==3){
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else{
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed=0;
        }
    }
    n5Click(){
        if(this.pressed==4){
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else{
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed=0;
        }
    }
    n6Click(){
        if(this.pressed==5){
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else{
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed=0;
        }
    }
    n7Click(){
        if(this.pressed==6){
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else{
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed=0;
        }
    }
    n8Click(){
        if(this.pressed==7){
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else{
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed=0;
        }
    }
    n9Click(){
        if(this.pressed==8){
            cc.audioEngine.playEffect(this.rightSE, false);
            this.pressed++;
        }
        else{
            cc.audioEngine.playEffect(this.wrongSE, false);
            this.pressed=0;
        }
    }
    playBGM(){
        cc.audioEngine.playMusic(this.bgm,true);
    }
}
